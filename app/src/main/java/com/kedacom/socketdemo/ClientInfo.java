package com.kedacom.socketdemo;

import java.util.List;

/**
 * <pre>
 *     作者：Caowj
 *     邮箱：caoweijian@kedacom.com
 *     日期：2020/6/4 17:23
 * </pre>
 */

class ClientInfo {
    private String userId;
    private String stationCode;
    private List<String> topics ;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public ClientInfo(String userId, String stationCode, List<String> topics) {
        this.userId = userId;
        this.stationCode = stationCode;
        this.topics = topics;
    }
}
