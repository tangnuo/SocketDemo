package com.kedacom.socketdemo;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * <pre>
 *     作者：Caowj
 *     邮箱：caoweijian@kedacom.com
 *     日期：2020/4/7 16:39
 * </pre>
 */

public class GsonUtil {
    public static String toGsonString(Object obj) {
        return new Gson().toJson(obj);
    }


    /**
     * 将Object类型的json返回值，转换成 JsonObject
     *
     * @param objectJson
     * @return
     */
    public static JsonObject toJsonObject(Object objectJson) {
        String jsonStr = toGsonString(objectJson);
        if (TextUtils.isEmpty(jsonStr)) {
            throw new RuntimeException("json字符串");
        }

        JsonElement jsonElement = new JsonParser().parse(jsonStr);
        if (jsonElement.isJsonNull()) {
            throw new RuntimeException("得到的jsonElement对象为空");
        }

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        return jsonObject;
    }


    /**
     * 转成bean
     */
    public static <T> T GsonToBean(String gsonString, Class<T> cls) {
        T t = new Gson().fromJson(gsonString, cls);
        return t;
    }

    public static <T> T GsonToBean(JsonObject jsonObject, Class<T> cls) {
        String gsonString = String.valueOf(jsonObject);//可以
//        String gsonString2 = toGsonString(jsonObject);//可以
//        String gsonString3 = jsonObject.toString();//不支持这种写法

        return GsonToBean(gsonString, cls);
    }

}
